package com.feign.currencyconversion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrencyConversionController {

    @Autowired
    private CurrencyExchangeProxy currencyExchangeProxy;

    @GetMapping("/currency-conversion-feign/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversion calculateCurrencyConversionFeign(@PathVariable String from, @PathVariable String to, @PathVariable Integer quantity) {

        CurrencyConversion currencyConversion = currencyExchangeProxy.getExchangeValue(from, to);

        currencyConversion.setQuantity(quantity);
        currencyConversion.setTotalCalculatedValue(quantity * currencyConversion.getConversionValue());

        return currencyConversion;
    }

}

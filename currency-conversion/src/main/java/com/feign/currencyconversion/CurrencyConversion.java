package com.feign.currencyconversion;

public class CurrencyConversion {

    private Long id;
    private String from;
    private String to;
    private Integer quantity;
    private Integer conversionValue;
    private Integer totalCalculatedValue;

    private String environmentPort;

    public CurrencyConversion(Long id, String from, String to, Integer quantity, Integer conversionValue, Integer totalCalculatedValue, String environmentPort) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.quantity = quantity;
        this.conversionValue = conversionValue;
        this.totalCalculatedValue = totalCalculatedValue;
        this.environmentPort = environmentPort;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getConversionValue() {
        return conversionValue;
    }

    public void setConversionValue(Integer conversionValue) {
        this.conversionValue = conversionValue;
    }

    public Integer getTotalCalculatedValue() {
        return totalCalculatedValue;
    }

    public void setTotalCalculatedValue(Integer totalCalculatedValue) {
        this.totalCalculatedValue = totalCalculatedValue;
    }

    public String getEnvironmentPort() {
        return environmentPort;
    }

    public void setEnvironmentPort(String environmentPort) {
        this.environmentPort = environmentPort;
    }
}

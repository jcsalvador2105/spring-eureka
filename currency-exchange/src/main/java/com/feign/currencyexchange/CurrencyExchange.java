package com.feign.currencyexchange;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class CurrencyExchange {

    @Id
    private Long id;
    @Column(name = "currency_from")
    private String from;
    @Column(name = "currency_to")
    private String to;
    private Integer conversionValue;

    private String environmentPort;

    public CurrencyExchange(Long id, String from, String to, Integer conversionValue) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.conversionValue = conversionValue;
    }

    public CurrencyExchange() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Integer getConversionValue() {
        return conversionValue;
    }

    public void setConversionValue(Integer conversionValue) {
        this.conversionValue = conversionValue;
    }

    public String getEnvironmentPort() {
        return environmentPort;
    }

    public void setEnvironmentPort(String environmentPort) {
        this.environmentPort = environmentPort;
    }
}

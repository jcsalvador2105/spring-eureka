package com.feign.currencyexchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyExchangeService {

    @Autowired
    private CurrencyExchangeRepository currencyExchangeRepository;


    public CurrencyExchange findByFromAndTo(String from, String to) {
        return currencyExchangeRepository.findByFromAndTo(from, to);
    }
}
